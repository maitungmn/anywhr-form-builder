import mongoose from "mongoose"
import { updateIfCurrentPlugin } from "mongoose-update-if-current";

export enum EFormTypes {
  static = 'Static',
  input = 'Input',
  dropdown = 'Dropdown',
  radio = 'Radio',
}

interface FormAttrs {
  id: string
  type: EFormTypes
  expiresAt: Date
  label: string
  options: string[]
  optionsValue: string[]
  content: string
}

interface FormDoc extends mongoose.Document {
  items: FormAttrs[]
  expiresAt: Date
  version: number
}

interface FormModel extends mongoose.Model<FormDoc> {
  build(attrs: FormAttrs[]): FormDoc
}

const formSchema = new mongoose.Schema({
  items: {
    type: [{
      id: String,
      type: {
        type: String,
        enum: Object.values(EFormTypes),
        required: true,
      },
      expiresAt: Date,
      label: String,
      options: [String],
      optionsValue: [String],
      content: String,
    }],
    required: true,
  },
  createdAt: {
    type: mongoose.Schema.Types.Date
  },
})

formSchema.set('versionKey', 'version')
formSchema.plugin(updateIfCurrentPlugin)

formSchema.statics.build = (attrs: FormAttrs) => {
  return new Form(attrs)
}

const Form = mongoose.model<FormDoc, FormModel>('Form', formSchema)

export { Form }