import express, { Request, Response } from "express";
import mongoose from "mongoose"
import { BaseRoute } from "./base-route";
import { body } from "express-validator";
import { Form } from "../models/form";

const router = express.Router()

const bodyValidator = [
  body('id')
    .not()
    .isEmpty()
    .custom((input: string) => mongoose.Types.ObjectId.isValid(input))
    .withMessage('FormId must be provided')
]

router.post(
  BaseRoute.FORM,
  bodyValidator,
  async (req: Request, res: Response) => {
    try {
      const expiration = new Date()

      const form = Form.build({
        ...req.body,
        createdAt: expiration,
      })
  
      await form.save()
  
      res.status(201).send(form)
    } catch(e) {
      console.error(e)
      res.status(400).send(e)
    }
    
  })

export { router as newFormRouter }