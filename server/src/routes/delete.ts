import express, { Response, Request } from "express";
import { BaseRoute } from "./base-route";
import { Form } from "../models/form";

const router = express.Router()

router.delete(`${BaseRoute.FORM}/:formId`, async (req: Request, res: Response) => {
  try {

    const { formId } = req.params
    const formItems = await Form.deleteOne({ id: formId })

    res.status(200).send(formItems)
  } catch (e) {
    console.error(e)
    res.status(400)
  }
})

export { router as deleteFormRouter }