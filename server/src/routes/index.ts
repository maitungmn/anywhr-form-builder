import express, { Response, Request } from "express";
import { BaseRoute } from "./base-route";
import { Form } from "../models/form";

const router = express.Router()

router.get(BaseRoute.FORM, async (req: Request, res: Response) => {
  const formItems = await Form.find({})

  res.status(200).send(formItems)
})

export { router as indexFormRouter }