import express from 'express'
import 'express-async-errors'
import { json } from 'body-parser'
import cookieSession from "cookie-session";
import cors from "cors"

import { errorHandler, NotFoundError, currentUser } from "@mttickets/common";

import { indexFormRouter } from "./routes";
import { newFormRouter } from "./routes/new";
import { deleteFormRouter } from "./routes/delete";
import { deleteAllFormRouter } from "./routes/delete-all";


const app = express()
app.set('trust proxy', true)
app.use(json())
app.use(
  cookieSession({
    signed: false,
    secure: process.env.NODE_ENV !== 'test',
  })
)
app.use(cors());
app.use(currentUser)

app.use(indexFormRouter)
app.use(newFormRouter)
app.use(deleteFormRouter)
app.use(deleteAllFormRouter)

app.all('*', async () => {
  throw new NotFoundError()
})

app.use(errorHandler)

export { app }