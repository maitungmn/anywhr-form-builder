## Anywhr Test
A simple Form Builder.
- [Demo](http://128.199.226.248:3050/)

## Motivation
A key part of engineering at Anywhr is building web services to support both travellers and trip curators. As part of this assignment, you are expected to build a survey form builder like Google Forms, Typeform etc.

[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg?style=flat)](https://github.com/feross/standard)
 
## Screenshots
![alt form-builder](https://i.imgur.com/JUpDLC8.png)

## Tech/framework used
Ex. -

<b>Built with</b>
- [Typescript](https://www.typescriptlang.org/)
- [ReactJs](https://reactjs.org/)
- [NodeJs](https://nodejs.org/)
- [ExpressJs](https://expressjs.com/)
- [MongoDB](https://www.mongodb.com/)
- [Nginx](https://www.nginx.com/)
- [Docker](https://www.docker.com/)

## Features
Able to create, export and view with form types:
- Dropdown
- Radio
- Input
- Static 

## Installation
- With Docker:
```$ docker-compose up```

- Local: 
Start both client & server by: ```$ npm run start``` (required mongo is running)

## API Reference
```GET: /api/form // Get all Form Profile```

```Post: /api/form // Create Form Profile```

```Delete: /api/form/:id // Delete Form Profile with id```

```Delete: /api/form // Delete All Form Profile```

## Tests
Not implement yet.

## Credits
Give proper credits. This could be a link to any repo which inspired you to build this project, any blogposts or links to people who contrbuted in this project. 

#### Anything else that seems useful

## License
A short snippet describing the license (MIT, Apache etc)

MIT © [Mai Tung]()