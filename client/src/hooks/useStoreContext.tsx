import { useContext } from 'react'
import { StoreContext } from '../context'

export default () => useContext(StoreContext)