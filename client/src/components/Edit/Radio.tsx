import React from "react"
import { Form, Input } from 'antd';
import uniqid from "uniqid"
import styles from '../../App.module.less';

import useStoreContext from "../../hooks/useStoreContext"
import { useIsMount } from "../../hooks/useIsMount"
import { TRadio } from "../../context/dto"
import { ITextArea } from "../../utils/formTypes"
import { createValueFromString } from "../../utils/createValueFromString"

import MovingButtons from "./MovingButtons"

interface TOption {
  [key: string]: string
}

const Radio = ({ itemIndex }: { itemIndex: number }): JSX.Element => {
  const { formItems, updateFormItems } = useStoreContext()
  const isMount = useIsMount();

  const [options, setOptions] = React.useState<string[]>([])
  const [optionsValue, setOptionsValue] = React.useState<string[]>([])
  const [label, setLabel] = React.useState<string>('')

  React.useEffect(() => {
    if (!isMount) return
    const {
      options: stateOption,
      optionsValue: stateOptionsValue,
      label: stateLabel,
    } = formItems![itemIndex] as TRadio
    if (!stateOption && !stateLabel) return
    setOptions(stateOption)
    setLabel(stateLabel)
    setOptionsValue(stateOptionsValue)
  }, []);

  React.useEffect(() => {
    const clonedFormItems = JSON.parse(JSON.stringify(formItems))
    clonedFormItems[itemIndex] = {
      ...clonedFormItems[itemIndex],
      label,
      options,
      optionsValue,
    }
    updateFormItems!(clonedFormItems)
  }, [options, optionsValue, label])


  const [form] = Form.useForm();

  const uniqueFormItemName: string = uniqid()

  const onCreateOption = (e: TOption) => {
    if (!e[uniqueFormItemName]) return
    const combinedOptions = [...options, e[uniqueFormItemName]]
    setOptions(combinedOptions)
    setOptionsValue(combinedOptions.map((i: string): string => createValueFromString(i)))
    form.resetFields()
  }

  const onLabelChange = ({ target: { value } }: ITextArea) => {
    setLabel(value)
  }

  return (
    <div>
      <div className={styles['edit-radio']}>
        <div>
          <p>Radio Label:</p>
          <Input value={label} onChange={onLabelChange} />
        </div>

        {options?.length ? (
          <ul>
            {options.map((o) => {
              const key = uniqid() + o + uniqueFormItemName
              return (
                <li key={key}>{o}</li>
              )
            })}
          </ul>
        ) : null}

        <Form form={form} onFinish={onCreateOption}>
          <Form.Item name={uniqueFormItemName} label="Add an option">
            <Input />
          </Form.Item>
        </Form>
      </div>
      <MovingButtons itemIndex={itemIndex} />
    </div>
  )
}

export default React.memo(Radio)