import React from "react"
import { Input } from 'antd';
import styles from '../../App.module.less';

import useStoreContext from "../../hooks/useStoreContext"
import { TInput } from "../../context/dto"
import { ITextArea } from "../../utils/formTypes"

import MovingButtons from "./MovingButtons"

const InputComponent = ({ itemIndex }: { itemIndex: number }): JSX.Element => {
  const { formItems, updateFormItems } = useStoreContext()

  const [inputValue, setInputValue] = React.useState<string>('')

  React.useEffect(() => {
    const { label: labelContent } = formItems![itemIndex] as TInput
    setInputValue(labelContent)
  }, [])

  React.useEffect((): void => {
    const clonedFormItems = JSON.parse(JSON.stringify(formItems))
    clonedFormItems[itemIndex] = {
      ...clonedFormItems[itemIndex],
      label: inputValue,
    }
    updateFormItems!(clonedFormItems)
  }, [inputValue])

  const onLabelChange = ({ target: { value } }: ITextArea) => {
    setInputValue(value)
  }

  return (
    <div>
      <div className={styles['edit-input']}>
        <p>Input Label:</p>
        <Input
          placeholder="Ex: Notes"
          value={inputValue}
          onChange={onLabelChange}
        />
      </div>
      <MovingButtons itemIndex={itemIndex} />
    </div>
  )
}

export default React.memo(InputComponent)