import React from "react"
import axios from "axios"
import styles from '../../App.module.less';

import { Button } from 'antd';

import useStoreContext from "../../hooks/useStoreContext"

import { TFormItem } from "../../context/dto"

import Static from "./Static"
import Input from "./Input"
import Dropdown from "./Dropdown"
import Radio from "./Radio"

const DynamicComponents = {
  Static,
  Input,
  Dropdown,
  Radio,
}

const Edit = (): JSX.Element => {

  const { formItems } = useStoreContext()

  const [isSavingFormProfile, setIsSavingFormProfile] = React.useState<boolean>(false)
  const [saveProfileMsg, setSaveProfileMsg] = React.useState<string>('')

  const onClickDownloadJSON = (): void => {
    const dataStr = JSON.stringify({ formItems })
    const dataUri = 'data:application/json;charset=utf-8,' + dataStr

    const exportFileDefaultName = 'form-builder.json'
    const linkElement = document.createElement('a')
    linkElement.setAttribute('href', dataUri);
    linkElement.setAttribute('download', exportFileDefaultName)
    linkElement.click();
  }

  const renderFormComponents = React.useCallback((): JSX.Element[] => {
    const results = formItems!.map((i: TFormItem, index: number): JSX.Element => {
      const key = i.type + i.id + index
      const TagName = DynamicComponents[i.type]
      return <TagName key={key} itemIndex={index} />
    })
    return results
  }, [formItems])

  const saveFormProfile = async (): Promise<void> => {
    if (!formItems?.length || !process.env.REACT_APP_QUERY_API) return
    try {
      setIsSavingFormProfile(true)
      const result = await axios.post(process.env.REACT_APP_QUERY_API, { items: formItems })
      console.log(result.data)
      setSaveProfileMsg('Saved!')
      setTimeout((): void => {
        setSaveProfileMsg('')
      }, 3000)
    } catch (e) {
      console.error(e)
      setSaveProfileMsg('Failed!')
      setTimeout((): void => {
        setSaveProfileMsg('')
      }, 3000)
    }
    setIsSavingFormProfile(false)
  }

  return (
    <div className={styles.edit}>
      <div className={styles['edit-content-main']}>
        {formItems?.length ? (
          <div className={styles['edit-content-main-items']}>
            {renderFormComponents()}
            <div className={styles['export-buttons']}>
              <Button
                type="primary"
                className={styles['export-json']}
                onClick={onClickDownloadJSON}
              >Export JSON</Button>
              <Button
                className={styles['save-json-profile']}
                loading={isSavingFormProfile}
                onClick={saveFormProfile}
              >Save JSON profile</Button>
            </div>
            {saveProfileMsg && (
              <p>{saveProfileMsg}</p>
            )}
          </div>
        ) : <div className={styles['bg-text-add']}>Add View</div>}
      </div>
    </div >
  )
}

export default React.memo(Edit)