import React from "react"
import { Button } from 'antd';
import styles from '../../App.module.less';

import { CaretUpOutlined, CaretDownOutlined } from '@ant-design/icons'

import useStoreContext from "../../hooks/useStoreContext"

enum EDirections {
  UP,
  DOWN
}

const MovingButtons = ({ itemIndex }: { itemIndex: number }): JSX.Element => {

  const {
    formItems,
    removeAType,
    reIndexType,
  } = useStoreContext()

  const onRemove = () => {
    removeAType!(itemIndex)
  }

  const onMoving = (direction: EDirections) => () => {
    if (formItems!.length <= 1) return

    if (direction === EDirections.UP) {
      reIndexType!(itemIndex, itemIndex - 1)
      return
    }

    if (itemIndex === formItems!.length - 1) {
      reIndexType!(itemIndex, 0)
      return
    }
    reIndexType!(itemIndex, itemIndex + 1)
  }

  return (
    <div className={styles['edit-moving-btn']}>
      <Button type="primary" danger onClick={onRemove}>Remove</Button>
      <div className={styles['edit-moving-btn-directions']}>
        <Button onClick={onMoving(EDirections.UP)}>
          <CaretUpOutlined />
        </Button>
        <Button onClick={onMoving(EDirections.DOWN)}>
          <CaretDownOutlined />
        </Button>
      </div>
    </div>
  )
}

export default MovingButtons