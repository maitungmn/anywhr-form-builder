import React from "react"
import { Input } from 'antd';
import styles from '../../App.module.less';

import useStoreContext from "../../hooks/useStoreContext"
import { TStatic } from "../../context/dto"
import { ITextArea } from "../../utils/formTypes"

import MovingButtons from "./MovingButtons"

const { TextArea } = Input;

const Static = ({ itemIndex }: { itemIndex: number }): JSX.Element => {

  const { formItems, updateFormItems } = useStoreContext()

  const [textAreaValue, setTextAreaValue] = React.useState<string>('')

  React.useEffect((): void => {
    const { content: stateContent } = formItems![itemIndex] as TStatic
    setTextAreaValue(stateContent)
  }, [])

  React.useEffect((): void => {
    const clonedFormItems = JSON.parse(JSON.stringify(formItems))
    clonedFormItems[itemIndex] = {
      ...clonedFormItems[itemIndex],
      content: textAreaValue,
    }
    updateFormItems!(clonedFormItems)
  }, [textAreaValue])

  const onInputChange = ({ target: { value } }: ITextArea): void => {
    setTextAreaValue(value)
  }

  return (
    <div>
      <div className={styles['edit-static']}>
        <p>Static Content: </p>
        <TextArea
          value={textAreaValue}
          onChange={onInputChange}
          autoSize={{ minRows: 3, maxRows: 5 }}
        />
      </div>
      <MovingButtons itemIndex={itemIndex} />
    </div>
  )
}

export default React.memo(Static)