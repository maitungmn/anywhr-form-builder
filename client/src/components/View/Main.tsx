import React from "react"
import styles from '../../App.module.less';

import { Modal, Button } from 'antd';

import useStoreContext from "../../hooks/useStoreContext"

import { TFormItem } from "../../context/dto"

import Static from "./Static"
import Input from "./Input"
import Dropdown from "./Dropdown"
import Radio from "./Radio"

const DynamicComponents = {
  Static,
  Input,
  Dropdown,
  Radio,
}

const Main = (): JSX.Element => {

  const { formItems, formValues } = useStoreContext()

  const [modalVisible, setModalVisible] = React.useState<boolean>(false)

  const renderFormComponents = React.useCallback((): JSX.Element[] => {
    const results = formItems!.map((i: TFormItem, index: number): JSX.Element => {
      const key = 'view' + i.type + i.id + index
      const TagName = DynamicComponents[i.type]
      // @ts-ignore
      return <TagName key={key} itemIndex={index} item={i} />
    })
    return results
  }, [formItems])

  return (
    <div className={styles['view-content-main']}>
      {!formItems?.length
        ? (
          <div className={styles['bg-text']}>Form View</div>
        )
        : (
          <div className={styles['view-content-main-items']}>
            {renderFormComponents()}
            <Button
              className={styles['export-json']}
              onClick={(): void => { setModalVisible(true) }}
            >Submit</Button>
          </div>
        )}
      <Modal
        title="Form Value"
        visible={modalVisible}
        onCancel={(): void => { setModalVisible(false) }}
        footer={[
          <Button key="back" onClick={(): void => { setModalVisible(false) }}>
            Return
          </Button>,
        ]}
      >
        <p>{JSON.stringify(formValues)}</p>
      </Modal>
    </div >
  )
}

export default React.memo(Main)