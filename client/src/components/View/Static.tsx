import React from "react"

import { TStatic } from "../../context/dto"

interface IProps {
  item: TStatic
}

const Static = ({ item }: IProps): JSX.Element => {

  return (
    <div>
      <p>{item.content}</p>
    </div>
  )
}

export default React.memo(Static)