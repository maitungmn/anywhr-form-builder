import React from "react"
import uniqid from "uniqid"
import { Select } from 'antd';
import { SelectValue } from 'antd/lib/select'
import styles from '../../App.module.less';

import useStoreContext from "../../hooks/useStoreContext"
import { TDropdown, TFormValuesGeneral } from "../../context/dto"
import { EFormTypes } from "../../utils/formTypes"

const { Option } = Select;

interface IProps {
  item: TDropdown,
  itemIndex: number,
}

const Dropdown = ({ item, itemIndex }: IProps): JSX.Element => {
  const { formValues, updateFormValues } = useStoreContext()

  const [selectedValue, setSelectedValue] = React.useState<SelectValue>()

  React.useEffect(() => {
    if (!formValues?.length || !formValues![itemIndex]) return
    const inputObj = formValues![itemIndex] as TFormValuesGeneral
    if (!inputObj || !inputObj.value) return
    setSelectedValue(inputObj.value)
  }, [itemIndex, formValues, selectedValue])

  const onChange = (value: SelectValue): void => {
    if (!value) return

    const formValuesCloned = JSON.parse(JSON.stringify(formValues))

    formValuesCloned[itemIndex] = {
      type: EFormTypes.dropdown,
      value,
    }
    updateFormValues!(formValuesCloned)
  }

  return (
    <div className={styles['view-dropdown']}>
      <p>{item.label}</p>
      {item.options?.length ? (
        <Select
          showSearch
          style={{ width: 200 }}
          placeholder="Select one"
          optionFilterProp="children"
          value={selectedValue}
          onChange={onChange}
        >
          {item.options.map((o, index) => {
            const uniqueFormItemName: string = uniqid()
            const key = uniqid() + o + uniqueFormItemName
            return (
              <Option
                key={key}
                value={item.optionsValue?.[index] || ''}
              >
                {o}
              </Option>
            )
          })}
        </Select>
      ) : ''}
    </div>
  )
}

export default React.memo(Dropdown)