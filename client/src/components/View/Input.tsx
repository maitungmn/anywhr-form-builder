import React from "react"
import { Input } from 'antd';
import styles from '../../App.module.less';

import useStoreContext from "../../hooks/useStoreContext"
import { TInput, TFormValuesGeneral } from "../../context/dto"
import { EFormTypes } from "../../utils/formTypes"

interface IProps {
  item: TInput,
  itemIndex: number,
}
const InputComponent = ({ item, itemIndex }: IProps): JSX.Element => {

  const { formValues, updateFormValues } = useStoreContext()

  const [inputValue, setInputValue] = React.useState<string>('')

  React.useEffect(() => {
    if (!formValues?.length || !formValues![itemIndex]) return
    const inputObj = formValues![itemIndex] as TFormValuesGeneral
    if (!inputObj || !inputObj.value) return
    setInputValue(inputObj.value)
  }, [itemIndex, formValues, inputValue])

  const onChange = ({ target: { value } }: React.ChangeEvent<HTMLInputElement>): void => {
    if (!value) return

    const formValuesCloned = JSON.parse(JSON.stringify(formValues))

    formValuesCloned[itemIndex] = {
      type: EFormTypes.input,
      value,
    }
    updateFormValues!(formValuesCloned)
  }
  return (
    <div className={styles['view-input']}>
      <p>{item.label}</p>
      <Input
        value={inputValue}
        onChange={onChange}
      />
    </div>
  )
}

export default React.memo(InputComponent)