import React from "react"
import uniqid from "uniqid"
import { Radio } from 'antd';
import { RadioChangeEvent } from 'antd/lib/radio'

import useStoreContext from "../../hooks/useStoreContext"
import { TRadio, TFormValuesGeneral } from "../../context/dto"
import { EFormTypes } from "../../utils/formTypes"


interface IProps {
  item: TRadio,
  itemIndex: number,
}

const RadioComponent = ({ item, itemIndex }: IProps): JSX.Element => {
  const { formValues, updateFormValues } = useStoreContext()

  const [radioValue, setRadioValue] = React.useState<string | undefined>(undefined)

  React.useEffect(() => {
    if (!formValues?.length || !formValues![itemIndex]) return
    const inputObj = formValues![itemIndex] as TFormValuesGeneral
    if (!inputObj || !inputObj.value) return
    setRadioValue(inputObj.value)
  }, [itemIndex, formValues, radioValue])

  const onChange = ({ target: { value } }: RadioChangeEvent) => {
    if (!value) return

    const formValuesCloned = JSON.parse(JSON.stringify(formValues))

    formValuesCloned[itemIndex] = {
      type: EFormTypes.radio,
      value,
    }
    updateFormValues!(formValuesCloned)
  }

  return (
    <div>
      <p>{item.label}</p>
      {item.options?.length ? (
        <Radio.Group onChange={onChange} value={radioValue}>
          {item.options.map((o, index) => {
            const uniqueFormItemName: string = uniqid()
            const key = uniqid() + o + uniqueFormItemName
            return (
              <Radio
                key={key}
                value={item.optionsValue?.[index] || ''}
              >
                {o}
              </Radio>
            )
          })}
        </Radio.Group>
      ) : ''}
    </div>
  )
}

export default React.memo(RadioComponent)