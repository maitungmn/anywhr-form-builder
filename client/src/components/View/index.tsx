import React from 'react';
import axios from "axios";
import styles from '../../App.module.less';

import {
  Select,
  Button,
} from 'antd';

import { formTypes } from "../../utils/formTypes"

import useStoreContext from "../../hooks/useStoreContext"
import { TFormProfile } from "../../context/dto"

import Main from "./Main"

const { Option } = Select;

const View = (): JSX.Element => {
  const { setSelectedType, formProfiles, updateFormItems } = useStoreContext()

  const [modifyType, setModifyType] = React.useState<string | undefined>(undefined)
  const [isLoadingRemoveAll, setIsLoadingRemoveAll] = React.useState<boolean>(false)

  const onCreateChange = (value: string) => {
    setModifyType(value)
  }

  const onCreate = (): void => {
    if (modifyType) {
      setSelectedType!(modifyType)
    }
    setModifyType(undefined)
  }

  const onRemoveAll = async (): Promise<void> => {
    if (!formProfiles?.length || !process.env.REACT_APP_QUERY_API) {
      console.error('No profiles is available!')
      return
    }
    setIsLoadingRemoveAll(true)
    try {
      await axios.delete(process.env.REACT_APP_QUERY_API)
    } catch (e) {
      console.error(e)
    }
    setIsLoadingRemoveAll(false)
  }

  const onSelectFormProfile = (value: string): void => {
    const profileSelected = formProfiles!.filter((i: TFormProfile) => i._id === value)
    if (!profileSelected?.length) return
    updateFormItems!(profileSelected[0].items)
  }

  return (
    <div className={styles.view}>
      <header className={styles['view-header']}>
        <div>
          <p className={styles['view-header-title']}>Select a sample form:</p>
          <div className={styles['view-header-select']}>
            <Select
              style={{ width: 200 }}
              placeholder="Select a person"
              optionFilterProp="children"
              onChange={onSelectFormProfile}
            >
              {formProfiles?.length && formProfiles.map((p: TFormProfile) => {
                return (
                  <Option key={p._id} value={p._id}>{p._id}</Option>
                )
              })}
            </Select>
            <Button
              onClick={onRemoveAll}
              loading={isLoadingRemoveAll}
              danger
            >
              Remove All Profiles
            </Button>
          </div>
        </div>

        <div className={styles['view-header-divider']}></div>

        <div>
          <p className={styles['view-header-title']}>Create new one:</p>
          <div className={styles['view-header-title-create']}>
            <Select
              style={{ width: 150 }}
              placeholder="Select a form"
              optionFilterProp="children"
              onChange={onCreateChange}
              value={modifyType}
            >
              {formTypes.map((i, index) => (
                <Option
                  className={styles['option']}
                  key={index}
                  value={i}
                >{i}</Option>
              ))}
            </Select>
            <Button type="primary" onClick={onCreate}>Add</Button>
          </div>

        </div>

      </header>

      <div className={styles['view-content']}>
        <Main />
      </div>
    </div>
  )
}

export default React.memo(View);