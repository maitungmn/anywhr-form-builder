export interface ITextArea {
  target: {
    value: string
  }
}

export enum EFormTypes {
  static = 'Static',
  input = 'Input',
  dropdown = 'Dropdown',
  radio = 'Radio',
}

export const formTypes = Object.values(EFormTypes)