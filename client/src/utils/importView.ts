import React from "react"

export enum Container {
  VIEW = 'View',
  EDIT = 'Edit',
}

export const importView = (subreddit: string, container: Container) =>
  React.lazy(() =>
    import(`../components/${container}/${subreddit}`).catch((e) => console.error('Loading component failed!', e))
  );