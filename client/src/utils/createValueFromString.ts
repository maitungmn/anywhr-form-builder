export const createValueFromString = (str: string): string => {
  return str.replace(/\s+/g, '-').toLowerCase();
}