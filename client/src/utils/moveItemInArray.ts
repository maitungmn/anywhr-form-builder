export const moveItemInArray = (arr: any[], old_index: number, new_index: number) => {
  const cloneArr = JSON.parse(JSON.stringify(arr))

  while (old_index < 0) {
    old_index += cloneArr.length;
  }

  while (new_index < 0) {
    new_index += cloneArr.length;
  }

  cloneArr.splice(new_index, 0, cloneArr.splice(old_index, 1)[0]);
  return cloneArr;
}