import { EFormTypes } from "../utils/formTypes"

export type TStatic = {
  id: string,
  type: EFormTypes.static,
  content: string,
}

export type TInput = {
  id: string,
  type: EFormTypes.input,
  label: string,
}

export type TRadio = {
  id: string,
  type: EFormTypes.radio,
  label: string,
  options: string[],
  optionsValue: string[],
}

export type TDropdown = {
  id: string,
  type: EFormTypes.dropdown,
  label: string,
  options: string[],
  optionsValue: string[],
}

export type TFormItem = TStatic | TInput | TRadio | TDropdown
export type TFormItems = TFormItem[]

export type TFormValuesGeneral = {
  id: string,
  type: EFormTypes,
  value: string,
}

export type TFormProfile = TFormItem & {
  _id: string,
  items: TFormItems,
  createdAt: Date,
  version: number,
}

export interface IState {
  setSelectedType(type: string): void,
  removeAType(index: number): void,
  reIndexType(index: number, nextIndex: number): void,
  formItems: TFormItems,
  updateFormItems: (items: TFormItems) => void,
  formValues: TFormValuesGeneral[],
  updateFormValues(items: TFormValuesGeneral[]): void,
  formProfiles: TFormProfile[]
}