import React, { createContext, PureComponent } from 'react'
import axios from "axios"
import uniqid from "uniqid"
import { moveItemInArray } from "../utils/moveItemInArray"
import { IState, TFormItems, TFormValuesGeneral } from "./dto"

export const StoreContext = createContext<Partial<IState>>({})

class StoreContextProvider extends PureComponent {
  state = {
    formItems: [],
    formValues: [],
    formProfiles: [],
  }

  async componentDidMount() {
    console.log('process.env.REACT_APP_ENV', process.env.REACT_APP_QUERY_API)
    if (!process.env.REACT_APP_QUERY_API) {
      console.error('Missing: process.env.REACT_APP_QUERY_API')
      return
    }
    try {
      const { data } = await axios.get(process.env.REACT_APP_QUERY_API)
      if (data?.length) {
        this.setState({ formProfiles: data })
      }
    } catch (e) {
      console.error(e)
    }
  }

  setSelectedType = (type: string): void => {
    this.setState({
      formItems: [...this.state.formItems, { type, id: uniqid() }],
      formValues: [...this.state.formValues, { type, id: uniqid() }],
    })
  }

  removeAType = (index: number): void => {
    this.setState({
      formItems: this.state.formItems.filter((_, typeIndex) => typeIndex !== index),
      formValues: this.state.formValues.filter((_, typeIndex) => typeIndex !== index),
    })
  }

  reIndexType = (index: number, nextIndex: number): void => {
    const reIndexedValues = moveItemInArray(
      this.state.formValues,
      index,
      nextIndex
    )

    const reIndexedItems = moveItemInArray(
      this.state.formItems,
      index,
      nextIndex
    )

    this.setState({
      formValues: reIndexedValues,
      formItems: reIndexedItems,
    })
  }

  updateFormItems = (items: TFormItems): void => {
    this.setState({ formItems: items })
  }

  updateFormValues = (items: TFormValuesGeneral[]): void => {
    this.setState({ formValues: items })
  }

  render(): JSX.Element {
    return (
      <StoreContext.Provider
        value={{
          setSelectedType: this.setSelectedType,
          removeAType: this.removeAType,
          reIndexType: this.reIndexType,
          formItems: this.state.formItems,
          updateFormItems: this.updateFormItems,
          formValues: this.state.formValues,
          updateFormValues: this.updateFormValues,
          formProfiles: this.state.formProfiles,
        }}
      >
        {this.props.children}
      </StoreContext.Provider>
    )
  }
}

export default StoreContextProvider