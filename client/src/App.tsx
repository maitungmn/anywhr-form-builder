import React from 'react';
import styles from './App.module.less';

import View from "./components/View"
import Edit from "./components/Edit"

import StoreContext from "./context/index"

function App(): JSX.Element {

  return (
    <StoreContext>
      <div className={styles.App}>
        <div className={styles.container}>
          <View />
          <Edit />
        </div>
      </div>
    </StoreContext>
  );
}

export default App;
